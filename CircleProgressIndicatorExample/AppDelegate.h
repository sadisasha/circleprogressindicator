//
//  AppDelegate.h
//  CircleProgressIndicatorExample
//
//  Created by Aleksandr Sadikov on 03.04.16.
//  Copyright © 2016 Aleksandr Sadikov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

