//
//  CircleProgressIndicator.h
//  CircleProgressIndicator
//
//  Created by Aleksandr Sadikov on 11.12.14.
//  Copyright © 2014 Aleksandr Sadikov. All rights reserved.
//

#import <UIKit/UIKit.h>
@import QuartzCore;

IB_DESIGNABLE
@interface CircleProgressIndicator : UIView

@property (nonatomic) IBInspectable CGFloat value;
@property (nonatomic) IBInspectable CGFloat maxValue;

@property (nonatomic) IBInspectable CGFloat progressRotationAngle;
@property (nonatomic) IBInspectable CGFloat progressAngle;

@property (nonatomic, strong) IBInspectable UIColor *progressColor;
@property (nonatomic, strong) IBInspectable UIColor *progressStrokeColor;
@property (nonatomic) IBInspectable CGFloat progressLineWidth;
@property (nonatomic) IBInspectable NSInteger progressCapType;

@property (nonatomic, strong) IBInspectable UIColor *emptyLineColor;
@property (nonatomic) IBInspectable CGFloat emptyLineWidth;
@property (nonatomic) IBInspectable NSInteger emptyCapType;

@end

@interface CircleProgressIndicatorLayer : CALayer

@property (nonatomic) CGFloat progressAngle;
@property (nonatomic) CGFloat progressRotationAngle;

@property (nonatomic) CGFloat value;
@property (nonatomic) CGFloat maxValue;

@property (nonatomic) CGFloat progressLineWidth;
@property (nonatomic, strong) UIColor *progressColor;
@property (nonatomic, strong) UIColor *progressStrokeColor;
@property (nonatomic) CGLineCap progressCapType; //{kCGLineCapButt=0, kCGLineCapRound=1, kCGLineCapSquare=2}

@property (nonatomic) CGFloat emptyLineWidth;
@property (nonatomic) CGLineCap emptyCapType;
@property (nonatomic, strong) UIColor *emptyLineColor;

@end