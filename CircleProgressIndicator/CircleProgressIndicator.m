//
//  CircleProgressIndicator.m
//  CircleProgressIndicator
//
//  Created by Aleksandr Sadikov on 11.12.14.
//  Copyright © 2014 Aleksandr Sadikov. All rights reserved.
//

#import "CircleProgressIndicator.h"
@import CoreGraphics;

@implementation CircleProgressIndicator

- (instancetype)init {
    
    self = [super init];
    
    if (self) {
        
        [self initView];
    }
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        
        [self initView];
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    
    self = [super initWithCoder:coder];
    
    if (self) {
        
        [self initView];
    }
    
    return self;
}

- (void)initView {
    
    //Without setting the content scale factor the layer would be pixelated
    [self setContentScaleFactor:[[UIScreen mainScreen] scale]];
    [self setValue:75.f];
    [self setMaxValue:100.f];
    
    [self setProgressRotationAngle:0.f];
    [self setProgressStrokeColor:[UIColor yellowColor]];
    [self setProgressColor:[UIColor yellowColor]];
    [self setProgressCapType:kCGLineCapRound];
    [self setProgressLineWidth:12.f];
    [self setProgressAngle:80.f];
    
    [self setEmptyLineColor:[UIColor lightGrayColor]];
    [self setEmptyLineWidth:16.f];
    [self setEmptyCapType:kCGLineCapRound];
}

#pragma mark - Getters & Setters

- (void)setValue:(CGFloat)value {
    
    self.progressLayer.value = value;
    
    if (value == 0) {
        
        [self.layer setNeedsDisplay];
    }
}

- (CGFloat)value {
    
    return self.progressLayer.value;
}

- (void)setMaxValue:(CGFloat)maxValue {
    
    self.progressLayer.maxValue = maxValue;

    if (maxValue == 0) {
        
        [self.layer setNeedsDisplay];
    }
}

- (CGFloat)maxValue {
    
    return self.progressLayer.maxValue;
}

- (void)setProgressLineWidth:(CGFloat)width {
    
    self.progressLayer.progressLineWidth = width;
}

- (CGFloat)progressLineWidth {
    
    return self.progressLayer.progressLineWidth;
}

- (void)setEmptyLineWidth:(CGFloat)width {
    
    self.progressLayer.emptyLineWidth = width;
}

- (CGFloat)emptyLineWidth {
    
    return self.progressLayer.emptyLineWidth;
}

- (void)setProgressColor:(UIColor *)color {
    
    self.progressLayer.progressColor = color;
}

- (UIColor *)progressColor {
    
    return self.progressLayer.progressColor;
}

- (void)setProgressStrokeColor:(UIColor *)color {
    
    self.progressLayer.progressStrokeColor = color;
}

- (UIColor *)progressStrokeColor {
    
    return self.progressLayer.progressStrokeColor;
}

- (void)setEmptyLineColor:(UIColor *)emptyLineColor {
    
    self.progressLayer.emptyLineColor = emptyLineColor;
}

- (UIColor *)emptyLineColor {
    
    return self.progressLayer.emptyLineColor;
}

- (void)setProgressAngle:(CGFloat)progressAngle {
    
    self.progressLayer.progressAngle = progressAngle;
}

- (CGFloat)progressAngle {
    
    return self.progressLayer.progressAngle;
}

- (void)setProgressRotationAngle:(CGFloat)progressRootationAngle {
    
    self.progressLayer.progressRotationAngle = progressRootationAngle;
}

- (CGFloat)progressRootationAngle {
    
    return self.progressLayer.progressRotationAngle;
}

- (void)setProgressCapType:(NSInteger)progressCapType {
    
    self.progressLayer.progressCapType = [self safeCapType:progressCapType];
}

- (NSInteger)progressCapType {
    
    return self.progressLayer.progressCapType;
}

- (void)setEmptyCapType:(NSInteger)emptyCapType {
    
    self.progressLayer.emptyCapType = [self safeCapType:emptyCapType];
}

- (NSInteger)emptyCapType {
    
    return self.progressLayer.emptyCapType;
}

- (CGLineCap)safeCapType:(NSInteger)type {
    
    if (kCGLineCapButt <= type && type <= kCGLineCapSquare) {
        
        return (CGLineCap)type;
    }
    
    return kCGLineCapRound;
}

#pragma mark - CALayer

- (CircleProgressIndicatorLayer *)progressLayer {
    
    return (CircleProgressIndicatorLayer *)self.layer;
}

+ (Class)layerClass {
    
    return [CircleProgressIndicatorLayer class];
}

@end

@implementation CircleProgressIndicatorLayer

@dynamic value;
@dynamic maxValue;
@dynamic progressLineWidth;
@dynamic progressColor;
@dynamic progressStrokeColor;
@dynamic emptyLineWidth;
@dynamic progressAngle;
@dynamic emptyLineColor;
@dynamic emptyCapType;
@dynamic progressCapType;
@dynamic progressRotationAngle;

#pragma mark - Drawing

- (void)drawInContext:(CGContextRef)ctx {
    [super drawInContext:ctx];
    
    UIGraphicsPushContext(ctx);
    
    CGSize size = CGRectIntegral(CGContextGetClipBoundingBox(ctx)).size;
    [self drawEmptyBar:size context:ctx];
    [self drawProgressBar:size context:ctx];
    
    UIGraphicsPopContext();
}

- (void)drawEmptyBar:(CGSize)rectSize context:(CGContextRef)context {
    
    if(self.emptyLineWidth <= 0) {
        
        return;
    }
    
    CGMutablePathRef arc = CGPathCreateMutable();
    
    CGPathAddArc(arc,
                 NULL,
                 rectSize.width / 2,
                 rectSize.height / 2,
                 MIN(rectSize.width, rectSize.height) / 2 - self.progressLineWidth,
                 (self.progressAngle / 100.f) * M_PI - ((-self.progressRotationAngle / 100.f) * 2.f + 0.5) * M_PI,
                 -(self.progressAngle / 100.f) * M_PI - ((-self.progressRotationAngle / 100.f) * 2.f + 0.5) * M_PI,
                 YES);

    CGPathRef strokedArc = CGPathCreateCopyByStrokingPath(arc, NULL, self.emptyLineWidth, (CGLineCap)self.emptyCapType, kCGLineJoinMiter, 10);

    CGContextAddPath(context, strokedArc);
    CGContextSetStrokeColorWithColor(context, self.emptyLineColor.CGColor);
    CGContextSetFillColorWithColor(context, self.emptyLineColor.CGColor);
    CGContextDrawPath(context, kCGPathFillStroke);
    
    CGPathRelease(arc);
    CGPathRelease(strokedArc);
}

- (void)drawProgressBar:(CGSize)rectSize context:(CGContextRef)context {
    
    if(self.progressLineWidth <= 0){
        
        return;
    }
    
    CGMutablePathRef arc = CGPathCreateMutable();
    
    CGPathAddArc(arc,
                 NULL,
                 rectSize.width / 2,
                 rectSize.height / 2,
                 MIN(rectSize.width, rectSize.height) / 2 - self.progressLineWidth,
                 (self.progressAngle / 100.f) * M_PI - ((-self.progressRotationAngle / 100.f) * 2.f + 0.5) * M_PI - (2.f * M_PI) * (self.progressAngle / 100.f) * (100.f - 100.f * self.value / self.maxValue) / 100.f,
                 -(self.progressAngle / 100.f) * M_PI - ((-self.progressRotationAngle / 100.f) * 2.f + 0.5) * M_PI,
                 YES);
    
    CGPathRef strokedArc = CGPathCreateCopyByStrokingPath(arc, NULL, self.progressLineWidth, (CGLineCap)self.progressCapType, kCGLineJoinMiter, 10);
    
    CGContextAddPath(context, strokedArc);
    CGContextSetFillColorWithColor(context, self.progressColor.CGColor);
    CGContextSetStrokeColorWithColor(context, self.progressStrokeColor.CGColor);
    CGContextDrawPath(context, kCGPathFillStroke);
    
    CGPathRelease(arc);
    CGPathRelease(strokedArc);
}

#pragma mark - Support animation methods

+ (BOOL)needsDisplayForKey:(NSString *)key {
    
    if ([key isEqualToString:@"value"]) {
        
        return YES;
    }
    
    return [super needsDisplayForKey:key];
}

@end